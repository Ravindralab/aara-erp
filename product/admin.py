from django.contrib import admin
from .models import Category, SubCategory, Product, ProductImage
from django.utils.html import format_html

# Register your models here.
class ProductImageAdmin(admin.StackedInline):
    model = ProductImage

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['title']

class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ['title','keywords','description','create_at','update_at']

class ProductAdmin(admin.ModelAdmin):
    list_display = ['product_name','quantity','description','image_tag','create_at','update_at']

    def image_tag(self,obj):
        image_str = '<img src="{0}" style="width: 45px; height:45px;" />'.format(obj.product_image.url)
        product_images = ProductImage.objects.filter(product=obj)

        if product_images:
            for pro_img in product_images:
                image_str += ' <img src="{0}" style="width: 45px; height:45px;" />'.format(pro_img.image.url)
        return format_html(image_str)


admin.site.register(Category,CategoryAdmin)
admin.site.register(SubCategory,SubCategoryAdmin)
admin.site.register(Product,ProductAdmin)
