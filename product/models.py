from django.db import models
from main_app.models import Store
from django.contrib.auth.models import User

# Create your models here.

class Group(models.Model):
    title = models.CharField(max_length=55, blank=True)

    def __str__(self):
        return self.title


class Category(models.Model):
    title = models.CharField(max_length=11,null=True)

    def __str__(self):
        return self.title

class SubCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    keywords = models.CharField(max_length=255)
    description = models.TextField(max_length=255)
    create_at=models.DateTimeField(auto_now_add=True)
    update_at=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    code = models.CharField(max_length=15)
    departmenet = models.CharField(max_length=33)
    brand = models.CharField(max_length=33)
    hsn = models.CharField(max_length=15)
    status = models.BooleanField(default=True)

    height = models.DecimalField(max_digits=10, default=0.0, decimal_places=2,null=True, blank=True)
    width = models.DecimalField(max_digits=10, default=0.0, decimal_places=2,null=True, blank=True)
    depth = models.DecimalField(max_digits=10, default=0.0, decimal_places=2,null=True, blank=True)

    vendor = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    store = models.ForeignKey(Store, on_delete=models.CASCADE, blank=True, null=True)
    product_name = models.CharField(max_length=100)

    product_image=models.FileField(upload_to='product_image', blank=True, null=True)
    quantity = models.IntegerField(default=0)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    MRP = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    Rack = models.CharField(max_length=30, blank=True)
    vat = models.DecimalField(max_digits=10, default=0.0, decimal_places=2)

    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.product_name

class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    image = models.FileField(upload_to='product_image')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'product_images'

    def __str__(self):
        return self.product.product_name
