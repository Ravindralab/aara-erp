from django.contrib import admin
from main_app.models import *

# Register your models here.
class Levels_admin(admin.ModelAdmin):
    list_display  = ('level_name', 'id')

class Role_admin(admin.ModelAdmin):
    list_display  = ('user', 'role_level', 'id',)

class Store_admin(admin.ModelAdmin):
    list_display  = ('user', 'store_name', 'store_address', 'store_mobile', 'id')

admin.site.register(Levels, Levels_admin)
admin.site.register(Role, Role_admin)
admin.site.register(Store, Store_admin)
